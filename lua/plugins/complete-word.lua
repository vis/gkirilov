-- complete word at primary selection location using vis-complete(1)

vis:map(vis.modes.INSERT, "<C-n>", function()
	local win = vis.win
	local file = win.file
	local pos = win.selection.pos
	if not pos then return end

	local range = file:text_object_word(pos > 0 and pos-1 or pos);
	if not range then return end
	if range.start == range.finish then return end

	local current = file:content(range)
	if not current then return end
	local prefix, suffix
	if range.finish > pos then
		prefix = current:sub(1, pos - range.start)
		suffix = current:sub(pos - range.start + 1)
	else
		prefix = current
		suffix = ""
	end

	vis:feedkeys("<vis-selections-save><Escape><Escape>")
	-- collect words starting with prefix
	vis:command("x/\\b" .. prefix .. "\\w+/")
	local candidates = {}
	for sel in win:selections_iterator() do
		-- TODO: use suffix to complete only the middle part of words
		local candidate = file:content(sel.range)
		if candidate ~= current then
			table.insert(candidates, candidate)
		end
	end
	vis:feedkeys("<Escape><Escape><vis-selections-restore>")
	if #candidates == 1 and candidates[1] == "\n" then return end
	candidates = table.concat(candidates, "\n")

	local cmd = "printf '" .. candidates .. "' | sort -u | vis-menu"
	local status, out, err = vis:pipe(cmd)
	if status ~= 0 or not out then
		if err then vis:info(err) end
		return
	end
	out = out:sub(#prefix + 1, #out - 1)
	file:insert(pos, out)
	win.selection.pos = pos + #out
	-- restore mode to what it was on entry
	vis.mode = vis.modes.INSERT
end, "Complete word in file")
